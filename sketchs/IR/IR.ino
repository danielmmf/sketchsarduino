#include <Arduino.h> //Bibliotecas para Infra Vermelho
#include <NECIRrcv.h>
#define IRPIN 8 // Pino de entrada de dado IR


NECIRrcv ir(IRPIN) ;

const byte ledPin11 = 11;
const byte ledPin13 = 13;

void setup(){
  Serial.begin(9600);
  Serial.println("Inicia Setup");
  
  pinMode(ledPin11, OUTPUT);
  pinMode(ledPin13, OUTPUT);
  ir.begin() ; //Inicia função IR
  Serial.println("Termina Setup");
}

void loop(){
  digitalWrite(ledPin11 , HIGH);
  digitalWrite(ledPin13 , LOW);
  unsigned long ircode=0; //Declaração de variaveis
  unsigned long aux=0;
  while (ir.available()) {
    digitalWrite(ledPin13 , HIGH);
    digitalWrite(ledPin11 , LOW);
    ircode = ir.read() ; // Leitura dos dados pino de entrada  “8″
    aux=(ircode);
    Serial.println(aux);
    delay(10);
    Serial.println("------------------------");
  }
  delay(1000);
}
